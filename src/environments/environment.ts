// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBfMIRwmPk5g06Vel38XgceBOQct0oIVAc",
    authDomain: "macoloc-70f65.firebaseapp.com",
    projectId: "macoloc-70f65",
    storageBucket: "macoloc-70f65.appspot.com",
    messagingSenderId: "406265022068",
    appId: "1:406265022068:web:e149b04954dfceede87321",
    measurementId: "G-4CEPYPNVDW"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
