import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { SigninPage } from 'src/app/modals/signin/signin.page';
import { User } from 'src/app/models/user';
import { UtilsService } from '../utils.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  constructor(
    public ngFireAuth: AngularFireAuth, public afStore: AngularFirestore, private utils: UtilsService,
    ) {
    this.ngFireAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
   }

  
  // Login in with email/password
  SignIn(email, password) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password)
    /*.then(result => 
      //  this.ngZone.run(() => {
      //     this.router.navigate(['dashboard']);
      //   }) 
      this.SetUserData(result.user)
    )*/
  }

  // Register user with email/password
  RegisterUser(email, password) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password)
    .then((result) => {
      //  this.ngZone.run(() => {
      //     this.router.navigate(['dashboard']);
      //   })
      console.log(result.user.uid);
      console.log(result.user.email);
      
      this.SetUserData(result.user);
      console.log(this.isLoggedIn);
      
    })
  }

  // Store user in localStorage
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afStore.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }
  
  // Returns true when user is looged in
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    
    return (user !== null /*&& user.emailVerified !== false*/) ? true : false;
  }

  get getUserEmail(): string {
    const user = JSON.parse(localStorage.getItem('user'));
    return user.email
  }

  // Sign-out 
  signOut() {
    /*return*/ this.ngFireAuth.signOut().then(() => {
      localStorage.removeItem('user');
      // this.router.navigate(['login']);
    })
    this.utils.presentModal(SigninPage)
  }
}
