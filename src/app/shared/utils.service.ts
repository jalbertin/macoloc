import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private modalController: ModalController) { }

  async presentModal(page) {
    const modal = await this.modalController.create({
      component: page,
      cssClass: 'modal-sign'
    });
    return await modal.present();
  }
}
