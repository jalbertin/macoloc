import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { SigninPage } from '../modals/signin/signin.page';
import { AuthService } from '../shared/auth/auth.service';
import { UtilsService } from '../shared/utils.service';
import { FormGroup, FormControl } from '@angular/forms'; //imports

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute, public modalController: ModalController, private authService: AuthService,
    private utils: UtilsService,
    ) {
      
      this.checkSignedIn()
     }

  ngOnInit() {
  }

  checkSignedIn(){
    if(!this.authService.isLoggedIn){
      this.utils.presentModal(SigninPage)
    }
  }

  signOut(){
    console.log("signOut !!!!!")
    this.authService.signOut()
  }

}
