import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/shared/auth/auth.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SignupPage } from '../signup/signup.page';
import { UtilsService } from 'src/app/shared/utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
  ionicForm: FormGroup;
  isSubmitted = false;

  constructor(
    private alertController: AlertController, private authService: AuthService, private formBuilder: FormBuilder,
    private utils: UtilsService, private modalController: ModalController, private router: Router,
  ) {
    console.log("this.authService.isLoggedIn",this.authService.isLoggedIn);
   }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    })
  }

  signup(){
    this.authService.RegisterUser("toto2@gmail.com","toto").then(function(result){
      console.log(result);
    }).catch(function(error){
      console.log(error);
      
    })
    console.log("se connecter !!");
  }

  get errorControl() {
    return this.ionicForm.controls;
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      this.authService.SignIn(this.ionicForm.value.email,this.ionicForm.value.password)
      .then( result =>
        /*function(result){
        console.log(result);*/
        this.modalController.dismiss()
      // this.router.navigate(['home'])
      // console.log("ok")
      
      // console.log(this.authService.isLoggedIn)
      ).catch(function(error){
        console.log("erreur", error);
      })
      
    }
  }
  showSignup(){
    this.utils.presentModal(SignupPage)
    this.modalController.dismiss()
  }
}
